package com.example.test2.controllers;

import com.example.test2.models.ResponseMessage;
import com.example.test2.models.jwtAuth.LoginRequest;
import com.example.test2.models.jwtAuth.LoginResponse;
import com.example.test2.services.security.JwtService;
import com.example.test2.services.security.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/login")
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @PostMapping(value="")
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest) {
        try{
            authenticate(loginRequest.getUsername(), loginRequest.getPassword());
            final UserDetails userDetails = userDetailsService
                    .loadUserByUsername(loginRequest.getUsername());

            final String token = jwtService.generateToken(userDetails);

            ArrayList<String> authorities = new ArrayList<>();
            userDetails.getAuthorities().forEach((auth) -> authorities.add(auth.toString()));

//            return new ResponseEntity("loged in", HttpStatus.OK);
            return new ResponseEntity(new LoginResponse(true, "success login", userDetails.getUsername(), token, authorities), HttpStatus.OK);
        }catch (Exception ex){
//            return new ResponseEntity("bad login", HttpStatus.BAD_REQUEST);
            return new ResponseEntity(new ResponseMessage(ex.getMessage()), HttpStatus.FORBIDDEN);
        }


    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            System.out.println("INVALID_CREDENTIALS : " + username + " " + password);
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
