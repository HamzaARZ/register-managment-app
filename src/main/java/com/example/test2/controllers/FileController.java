package com.example.test2.controllers;

import com.example.test2.models.File;
import com.example.test2.models.FileUpload;
import com.example.test2.models.ResponseMessage;
import com.example.test2.services.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class FileController {

    @Autowired
    private FileService fileService;

//    @RequestMapping(value = "/files", method = RequestMethod.GET)
//    public ArrayList<File> getAllFiles() {
//        return fileService.getAllFiles();
//    }
//
//    @GetMapping(value="/files{id}")
//    public File getFile(@PathVariable UUID id){
//        return fileService.getFile(id);
//    }
//
//
//    //@PostMapping(value="/files")
//    private void addFile(@RequestBody File file){
//        fileService.addFile(file);
//    }
//
//    @PutMapping(value="/files")
//    public void updateFile(@RequestBody File file){
//        fileService.updateFile(file);
//    }


    @PreAuthorize("hasAuthority('file:read')")
    @GetMapping(value = "/files/{projectId}")
    public List<String> getProjectFiles(@PathVariable UUID projectId) {
        return fileService.getProjectFiles(projectId);
    }


    // stockage des fichiers
    @PreAuthorize("hasAuthority('file:write')")
    @PostMapping(value = "/upload_file/{project_id}")
    public String uploadFile(@RequestParam("file") MultipartFile file, @PathVariable("project_id") String projectId) {
        return fileService.uploadFile(file, projectId);
    }

    @PreAuthorize("hasAuthority('file:write')")
    @PostMapping(value = "/upload_files/{project_id}")
    public ResponseEntity<ResponseMessage> uploadFiles(@RequestParam("file") MultipartFile[] files, @PathVariable("project_id") String projectId){
        try{
            return new ResponseEntity(new ResponseMessage(fileService.uploadFiles(files, projectId)), HttpStatus.OK);
        }catch(IOException ex){
            return new ResponseEntity(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }

    }

    //@PreAuthorize("hasAuthority('file:read')")
    @GetMapping(value = "/download_file/{folderName}/{fileName}")
    public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable String folderName, @PathVariable String fileName) throws IOException {
        return fileService.downloadFile(folderName, fileName);
    }

    @PreAuthorize("hasAuthority('file:write')")
    @DeleteMapping(value = "/files/{folderName}/{fileName}")
    public void deleteFile(@PathVariable UUID folderName,  @PathVariable String fileName){
        fileService.deleteFile(folderName, fileName);
    }

}
