package com.example.test2.controllers;

import com.example.test2.services.PersonService;
import com.example.test2.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.UUID;


@RestController
@RequestMapping("/persons")
public class PersonController {

    @Autowired
    private PersonService personService;


    @RequestMapping(value="", method= RequestMethod.GET)
    public ArrayList<Person> getAllPersons() {
        return personService.getAllPersons();
    }


    @GetMapping(value = "/{id}")
    public Person getPerson(@PathVariable UUID id) {

        return personService.getPerson(id);
    }


    @PostMapping(value="")
    public void addPerson(@RequestBody Person person){
        System.out.println("Posted person : " + person);
        personService.addPerson(person);
    }



    @PutMapping(value="")
    public void updatePerson(@RequestBody Person person){
        personService.updatePerson(person);
    }


    @DeleteMapping(value = "/{id}")
    public void deletePerson(@PathVariable UUID id){
        personService.deletePerson(id);
    }
}