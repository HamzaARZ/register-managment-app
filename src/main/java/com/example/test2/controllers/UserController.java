package com.example.test2.controllers;

import com.example.test2.handlers.HandleSomeExceptions;
import com.example.test2.handlers.UsernameAlreadyExistException;
import com.example.test2.models.ResponseMessage;
import com.example.test2.models.User;
import com.example.test2.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('user:read')")
    @GetMapping(value="")
    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }

    @PreAuthorize("hasAuthority('user:read')")
    @GetMapping(value = "/{username}")
    public ResponseEntity<?> getUserByUsername(@PathVariable String username){
        try {
            Optional<User> optionalUser = userService.getUserByUsername(username);

            User user = optionalUser
                    .orElseThrow(() -> new UsernameNotFoundException("No user Found with username : "
                            + username));
            return new ResponseEntity(user, HttpStatus.OK);
        }catch(IllegalArgumentException ex){
            return new ResponseEntity(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }catch(UsernameNotFoundException ex){
            return new ResponseEntity(new ResponseMessage(ex.getMessage()), HttpStatus.NOT_FOUND);
        }
    }




    @PostMapping(value="")
    public ResponseEntity<?> addUser(@Valid @RequestBody User user){
        System.out.println("Adding user : " + user);
        try{
            return new ResponseEntity(userService.addUser(user), HttpStatus.OK);
        } catch (UsernameAlreadyExistException ex){
            return new ResponseEntity(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (IllegalArgumentException ex){
            return new ResponseEntity(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            return new ResponseEntity(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }


    @PreAuthorize("hasAuthority('user:read')")
    @PutMapping(value = "")
    public ResponseEntity<?> updateOwnUserAcc(@RequestBody User user,
                                        @RequestHeader("Authorization") String token){
        try{
            return new ResponseEntity(userService.updateOwnUserAcc(user, token),   HttpStatus.OK);
        } catch (UsernameAlreadyExistException ex){
            return new ResponseEntity(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('user:write')")
    @PutMapping(value = "/{username}")
    public ResponseEntity<?> updateUser(@PathVariable String username, @RequestBody User user){
        try{
            return new ResponseEntity(userService.updateUser(username, user), HttpStatus.OK);
        }catch(UsernameNotFoundException ex){
            return new ResponseEntity(new ResponseMessage(ex.getMessage()), HttpStatus.NOT_FOUND);
        }catch(IllegalArgumentException ex){
            return new ResponseEntity(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch(UsernameAlreadyExistException ex){
            return new ResponseEntity(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('user:write')")
    @PutMapping(value = "activate/{username}")
    public ResponseEntity<ResponseMessage> activateAcc(@PathVariable String username){
        try{
            userService.activateAcc(username);
            return new ResponseEntity(new ResponseMessage("user account '" + username + "' is activated now"), HttpStatus.OK);
        }catch(UsernameNotFoundException ex){
            return new ResponseEntity(new ResponseMessage(ex.getMessage()), HttpStatus.NOT_FOUND);
        }
    }

    @PreAuthorize("hasAuthority('user:write')")
    @PutMapping(value = "disable/{username}")
    public ResponseEntity<ResponseMessage> disableAcc(@PathVariable String username){
        try{
            userService.disableAcc(username);
            return new ResponseEntity(new ResponseMessage("user account '" + username + "' is disabled now"), HttpStatus.OK);
        }catch(UsernameNotFoundException ex){
            return new ResponseEntity(new ResponseMessage(ex.getMessage()), HttpStatus.NOT_FOUND);
        }
    }


    @PreAuthorize("hasAuthority('user:write')")
    @DeleteMapping(value = "/{username}")
    public ResponseEntity<ResponseMessage> deleteUser(@PathVariable String username){
        try{
            userService.deleteUserById(username);
            return new ResponseEntity(new ResponseMessage("user with username '" + username + "' has been deleted"), HttpStatus.OK);
        }catch(NoSuchElementException ex){
            return new ResponseEntity(new ResponseMessage("no user found with username '" + username + "'"), HttpStatus.NOT_FOUND);
        }catch(IllegalArgumentException ex){
            return new ResponseEntity(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}