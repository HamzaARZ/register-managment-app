package com.example.test2.controllers;

import com.example.test2.models.Client;
import com.example.test2.models.ResponseMessage;
import com.example.test2.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;


@RestController
@RequestMapping("/clients")
public class ClientController{

    @Autowired
    private ClientService clientService;

    @PreAuthorize("hasAuthority('client:read')")
    @GetMapping(value="")
    public List<Client> getNotDeletedClients(){
        return clientService.getNotDeletedClients();
    }

    @PreAuthorize("hasAuthority('client:read')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getClient(@PathVariable UUID id){
        try{
            return new ResponseEntity(clientService.getClient(id), HttpStatus.OK);
        }catch(NoSuchElementException ex){
            return new ResponseEntity(new ResponseMessage("client not found"), HttpStatus.NOT_FOUND);
        }
    }


    // @EnableGlobalMethodSecurity(prePostEnabled = true) in ApplicationSecurityConfig
    // hasRole('ROLE_')  hasAnyRole('ROLE_')  hasAuthority('permission')  hasAnyAuthority('permission')
    //@PreAuthorize("hasAuthority('USER')")
    @PreAuthorize("hasAuthority('client:write')")
    @PostMapping(value="")
    public void addClient(@Valid @RequestBody Client client){
        clientService.addClient(client);
    }

    @PreAuthorize("hasAuthority('client:write')")
    @PutMapping(value = "")
    public void updateClient(@RequestBody Client client){
        clientService.updateClient(client);
    }

    @PreAuthorize("hasAuthority('client:write')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteClient(@PathVariable UUID id){
        try{
            clientService.deleteClientById(id);
            return new ResponseEntity(new ResponseMessage("client deleted"), HttpStatus.OK);
        }catch(NoSuchElementException ex){
            return new ResponseEntity(new ResponseMessage("client not found"), HttpStatus.NOT_FOUND);
        }
    }
}