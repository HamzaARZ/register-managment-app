package com.example.test2.controllers;


import com.example.test2.models.Project;
import com.example.test2.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class ProjectController {

    @Autowired
    private ProjectService projectService;


    @PreAuthorize("hasAuthority('project:read')")
    @GetMapping(value = "/projects")
    public List<Project> getNotDeletedProjects(){
        return projectService.getNotDeletedProjects();
    }

    @PreAuthorize("hasAuthority('project:read')")
    @GetMapping(value = "/projects/{id}")
    public Project getProject(@PathVariable UUID id){
        return projectService.getProject(id);
    }

    @PreAuthorize("hasAuthority('project:write')")
    @PostMapping(value = "/projects")
    public void addProject(@RequestBody Project project){
        projectService.addProject(project);
    }

    @PreAuthorize("hasAuthority('project:read')")
    @GetMapping(value = "/clients/{clientId}/projects")
    public List<Project> getProjectsByClientId(@PathVariable UUID clientId) {
        return projectService.getProjectsByClientId(clientId);
    }

    @PreAuthorize("hasAuthority('project:write')")
    @PutMapping(value="/projects")
    public void updateProject(@RequestBody Project project){
        projectService.updateProject(project);
    }


    @PreAuthorize("hasAuthority('project:write')")
    @DeleteMapping(value = "/projects/{id}")
    public void deleteProjectById(@PathVariable UUID id){
        projectService.deleteProjectById(id);
    }



}
