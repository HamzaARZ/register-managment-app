package com.example.test2.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class AliveController {
    @GetMapping("/alive")
    public ResponseEntity alive() {
        return ResponseEntity.ok(new Date());
    }
}
