package com.example.test2.services;


import com.example.test2.models.Person;
import com.example.test2.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.UUID;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;


    public ArrayList<Person> getAllPersons(){
        ArrayList<Person> persons = new ArrayList<>();

        personRepository.findAll().forEach(persons::add);
        return persons;
    }


    public Person getPerson(UUID id){

        return personRepository.findById(id).get();
    }


    public void addPerson(Person person){
        personRepository.save(person);
    }

    public void updatePerson(Person person){
        personRepository.save(person);
    }

    public void deletePerson(UUID id){
        personRepository.deleteById(id);
    }




}
