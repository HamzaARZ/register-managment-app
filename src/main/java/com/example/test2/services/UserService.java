package com.example.test2.services;

import com.example.test2.handlers.UsernameAlreadyExistException;
import com.example.test2.models.User;
import com.example.test2.models.jwtAuth.UserRole;
import com.example.test2.repositories.UserRepository;
import com.example.test2.services.security.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class UserService {

//    @Autowired
//    private PasswordEncoder passwordEncoder;
// !!!!!!!!!!!!!!!!!!!!!!!!!
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtService jwtService;


    public Optional<User> getUserByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public List<User> getAllUsers(){
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        return users;
    }

    public User addUser(User user) throws UsernameAlreadyExistException, IllegalArgumentException{
        if(isExist(user.getUsername()))
            throw new UsernameAlreadyExistException("username '" + user.getUsername() + "' is already in use");

        if(!user.getRole().toString().equals("USER") && !user.getRole().toString().equals("MANAGER"))
            throw new IllegalArgumentException ("role should be 'USER' or 'MANAGER'");

        user.setPassword(passwordEncoder().encode(user.getPassword()));
        user.setEnabled(user.getRole().toString().equals("USER"));
        return userRepository.save(user);
    }




    @Transactional(rollbackFor = Throwable.class, propagation = Propagation.REQUIRED)
    public User updateOwnUserAcc(User newUser, String token)throws UsernameAlreadyExistException{
        token = token.substring(7);

        String username = jwtService.getUsernameFromToken(token);

        User user = getUserByUsername(username).get();


        // cant change role or enabled properties
        newUser.setEnabled(user.getEnabled());
        newUser.setRole(user.getRole());

        if(!newUser.getUsername().equals(username) && isExist(newUser.getUsername())){
            throw new UsernameAlreadyExistException("username '" + newUser.getUsername() + "' is already in use");
        }else{
            newUser.setPassword(passwordEncoder().encode(newUser.getPassword()));
            userRepository.deleteById(username);
            return userRepository.save(newUser);
        }

    }

    public void deleteUserById(String username)throws NoSuchElementException {
        User user = getUserByUsername(username).get();
        if(user.getRole() != UserRole.ADMIN) {
            userRepository.deleteById(username);
        }else{
            throw new IllegalArgumentException("can not delete ADMIN account");
        }
    }

    public Boolean isExist(String username){
        return userRepository.existsById(username);
    }

    public User updateUser(String username, User user)throws UsernameAlreadyExistException{
        if(!isExist(username))
            throw new UsernameNotFoundException("No user Found with username : " + username);

        if(getUserByUsername(username).get().getRole() == UserRole.ADMIN)
            throw new IllegalArgumentException("can not change ADMIN account");

        if(!user.getUsername().equals(username) && isExist(user.getUsername()))
            throw new UsernameAlreadyExistException("username '" + user.getUsername() + "' is already in use !!");


        user.setEnabled(getUserByUsername(username).get().getEnabled());

        user.setPassword(passwordEncoder().encode(user.getPassword()));
        userRepository.deleteById(username);
        return userRepository.save(user);
    }

    public void activateAcc(String username) {
        Optional<User> userOptional = getUserByUsername(username);
        User user = userOptional
                .orElseThrow(() -> new UsernameNotFoundException("No user Found with username : "
                        + username));

        user.setEnabled(true);
        userRepository.save(user);
    }
    public void disableAcc(String username) {
        Optional<User> userOptional = getUserByUsername(username);
        User user = userOptional
                .orElseThrow(() -> new UsernameNotFoundException("No user Found with username : "
                        + username));

        user.setEnabled(false);
        userRepository.save(user);
    }
}


