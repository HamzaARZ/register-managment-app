package com.example.test2.services;


import com.example.test2.models.Client;
import com.example.test2.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;


//    public List<Client> getAllClients(){
//        List<Client> clients = new ArrayList<>();
//
//        clientRepository.findAll().forEach(clients::add);
//        return clients;
//    }

    public List<Client> getNotDeletedClients() {
        return clientRepository.getNotDeletedClients();
    }


    // TODO: should get only not deleted client
    public Client getClient(UUID id) throws NoSuchElementException{
        Client client = clientRepository.getNotDeletedClientById(id.toString());
        System.out.println(client);
        if(client != null){
            return client;
        }else{
            throw new NoSuchElementException();
        }
    }

    @Transactional(rollbackFor = Throwable.class, propagation = Propagation.REQUIRED)
    public Client addClient(Client client){ return clientRepository.save(client); }

    public void updateClient(Client client){
        clientRepository.save(client);
    }



    public void deleteClientById(UUID id)throws NoSuchElementException{
        clientRepository.deleteClientById(id.toString());
    }

    public boolean isExist(UUID id){
        return clientRepository.existsById(id);
    }
}