package com.example.test2.services;

import com.example.test2.models.File;
import com.example.test2.repositories.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class FileService {
    private static final String DIRECTORY = "C:\\Users\\Lenovo\\Desktop\\test2-files";
    private static final String TRASH_DIRECTORY = "C:\\Users\\Lenovo\\Desktop\\test2-files\\trash";


    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private ProjectService projectService;


//    public ArrayList<File> getAllFiles(){
//        ArrayList<File> files = new ArrayList<>();
//
//        fileRepository.findAll().forEach(files::add);
//        return files;
//    }
//
//    public File getFile(UUID id){
//        return fileRepository.findById(id).get();
//    }
//
//    public void addFile(File file){
//        Project project = file.getProject();
//        UUID projectId = project.getId();
//
//        if (projectId != null) {
//            if (projectService.isExist(projectId))
//                fileRepository.save(file);
//        }
//    }
//
//    public void updateFile(File file){
//        fileRepository.save(file);
//    }




    public void createProjectFolder(UUID id){
        String path = DIRECTORY + "\\" + id;
        String trashPath = TRASH_DIRECTORY + "\\" + id;

        java.io.File projectFolder = new java.io.File(path);
        java.io.File trashFolder = new java.io.File(trashPath);
        projectFolder.mkdir();
        trashFolder.mkdir();
    }



    public void moveFolderToTrash(UUID folderName){
        // move all files to trash folder
        for(String file: getProjectFiles(folderName)){
            deleteFile(folderName, file);
        }

        // delete the folder
        String projectFolderPath = DIRECTORY + "\\" + folderName;
        java.io.File projectFolder = new java.io.File(projectFolderPath);
        projectFolder.delete();


//        String projectFolderPath = DIRECTORY + "\\" + id;
//        String trashFolderPath = TRASH_DIRECTORY + "\\" + id;
//
//        java.io.File projectFolder = new java.io.File(projectFolderPath);
//        java.io.File trashFolder = new java.io.File(trashFolderPath);
//
//        try {
//            Files.move(projectFolder.toPath(), trashFolder.toPath(), StandardCopyOption.REPLACE_EXISTING);
//            System.out.println("Directory moved successfully.");
//        }
//        catch (IOException ex) {
//            ex.printStackTrace();
//        }
    }




    // move file to trash directory
    public void deleteFile(UUID folderName, String fileName){
        String trashFilePath = TRASH_DIRECTORY + "\\" + folderName + "\\" + fileName;
        String filePath = DIRECTORY + "\\" + folderName + "\\" + fileName;

        java.io.File file = new java.io.File(filePath);
        java.io.File trashFolder = new java.io.File(trashFilePath);

        try {
            Files.move(file.toPath(), trashFolder.toPath(), StandardCopyOption.REPLACE_EXISTING);
            System.out.println("file moved successfully.");
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }

//        file.delete();
    }

    public ArrayList<String> getProjectFiles(UUID projectId){
        //TODO Error if the directory not found !!
        java.io.File f = new java.io.File(DIRECTORY + "\\" + projectId);

        // Populates the array with names of files and directories
        return new ArrayList<String>(Arrays.asList(f.list()));
    }





    // stockage des fichiers
    public String uploadFile(MultipartFile file, String folderName){
        String directory = DIRECTORY + "\\" + folderName;
        Path fileNameAndPath = Paths.get(directory,file.getOriginalFilename());
        System.out.println(file.getOriginalFilename());

        try {
            Files.write(fileNameAndPath, file.getBytes());
            return "successfully uploaded : " + file.getOriginalFilename();
        } catch (IOException e) {
            e.printStackTrace();
            return "Error : " + e.getMessage();
        }
    }


    public String uploadFiles(MultipartFile[] files, String folderName) throws IOException{
        Path fileNameAndPath;
        String directory = DIRECTORY + "\\" + folderName;
        String stringToReturn = "successfully uploaded : \n";

        for (MultipartFile file : files) {
            fileNameAndPath = Paths.get(directory, file.getOriginalFilename());
            try {
                Files.write(fileNameAndPath, file.getBytes());
                stringToReturn += "\t-" + file.getOriginalFilename() + "\n";
            } catch (IOException ex) {
                throw new IOException(ex.getMessage());
            }
        }
        return stringToReturn;
    }


    public ResponseEntity<ByteArrayResource> downloadFile(String folderName, String fileName){

        String directory = DIRECTORY + "\\" + folderName + "\\" + fileName;

        MediaType mediaType;
        String mineType = servletContext.getMimeType(fileName);
        try {
            mediaType = MediaType.parseMediaType(mineType);
            mediaType = mediaType;
        } catch (Exception e) {
            mediaType = MediaType.APPLICATION_OCTET_STREAM;
        }

        Path path = Paths.get(directory);
        byte[] data = new byte[0];

        try {
            data = Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayResource resource = new ByteArrayResource(data);

        return ResponseEntity.ok()
                // Content-Disposition
                                                                      //inline
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + path.getFileName().toString())
                .contentType(mediaType)
                // Content-Lengh
                .contentLength(data.length)
                .body(resource);
    }
}