package com.example.test2.services;


import com.example.test2.models.Client;
import com.example.test2.models.Project;

import com.example.test2.repositories.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private ClientService clientService;
    @Autowired
    private FileService fileService;

    public List<Project> getNotDeletedProjects(){

        List<Project> projects = new ArrayList<>();

        projectRepository.getNotDeletedProjects().forEach(projects::add);
        return projects;
    }


    public Project getProject(UUID id){

        return projectRepository.findById(id).get();
    }


    public List<Project> getProjectsByClientId(UUID clientId){
        return projectRepository.findByClientId(clientId);
    }


    @Transactional
    public void addProject(Project project){
        Client client = project.getClient();
        UUID id = client.getId();


        if (id == null){
            client = clientService.addClient(client);
            project.setClient(client);
            project = projectRepository.save(project);
            fileService.createProjectFolder(project.getId());


        }else {
            if(clientService.isExist(id)) {
                project = projectRepository.save(project);
                fileService.createProjectFolder(project.getId());
            }else
                System.out.println("client id not found !!");
        }

    }

    public void updateProject(Project project){
        projectRepository.save(project);
    }

    public void deleteProjectById(UUID id){
        projectRepository.deleteProjectById(id.toString());
        // delete folder
        fileService.moveFolderToTrash(id);
    }

    public boolean isExist(UUID id){
        return projectRepository.existsById(id);
    }


}
