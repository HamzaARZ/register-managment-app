package com.example.test2.handlers;

public class UsernameAlreadyExistException extends Exception {

    private String message;

    public UsernameAlreadyExistException(String message) {
        this.message = message;
    }



    @Override
    public String getMessage() {
        return message;
    }
}
