package com.example.test2.repositories;

import com.example.test2.models.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface ClientRepository extends JpaRepository<Client, UUID> {

    public List<Client> findByNom(String nom);

    @Query(value = "SELECT * FROM client where deleted_at is null", nativeQuery = true)
    List<Client> getNotDeletedClients();

    @Query(value = "SELECT * FROM client where deleted_at is null and client_id = :id", nativeQuery = true)
    Client getNotDeletedClientById(@Param("id") String id);



    @Transactional
    @Modifying
    @Query(value = "update client set deleted_at = now() where client_id = :id", nativeQuery = true)
    public void deleteClientById(@Param("id") String id);


//    @Query(
//            value = "SELECT * FROM client where client_id = ?1", nativeQuery = true)
//    List<Client> findByIdAndNomm(String id);

//    @Query(
//            value = "SELECT * FROM client where client_id = ?1 and nom = ?2", nativeQuery = true) //:id    :nom
//    List<Client> findByIdAndNomm(String id, String nom);

}
