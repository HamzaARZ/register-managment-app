package com.example.test2.repositories;


import com.example.test2.models.Project;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    public List<Project> findByClientId(UUID clientId);



    @Query(value = "select * from project where deleted_at is null", nativeQuery = true)
    List<Project> getNotDeletedProjects();

    @Transactional
    @Modifying
    @Query(value = "update project set deleted_at = now() where project_id = ?1", nativeQuery = true)
    public void deleteProjectById(String id);

}