package com.example.test2.repositories;

import com.example.test2.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
    public Optional<User> findByUsername(String userName);
}

