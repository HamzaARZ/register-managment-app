package com.example.test2.repositories;

import com.example.test2.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface PersonRepository extends JpaRepository<Person, UUID> {
    public List<Person> findByNom(String nom);
}
