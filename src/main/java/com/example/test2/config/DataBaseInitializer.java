package com.example.test2.config;

import com.example.test2.models.User;
import com.example.test2.models.jwtAuth.UserRole;
import com.example.test2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
//@ConditionalOnProperty(name = "app.db-init", havingValue = "true")
// should add 'app.db-init' property in application.properties
public class DataBaseInitializer implements CommandLineRunner {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public void run(String... args) throws Exception {

        if(userRepository.count() == 0){
            User admin = new User("hamza", passwordEncoder.encode("hamza123"), UserRole.ADMIN, true);
            userRepository.save(admin);
            System.out.println("admin added");
        }



    }
}
