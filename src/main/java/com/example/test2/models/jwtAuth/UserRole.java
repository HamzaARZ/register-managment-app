package com.example.test2.models.jwtAuth;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.test2.models.jwtAuth.UserPermission.*;

public enum UserRole {
    ADMIN(Arrays.asList(
            CLIENT_READ, CLIENT_WRITE,
            PROJECT_READ, PROJECT_WRITE,
            FILE_READ, FILE_WRITE,
            USER_READ, USER_WRITE
    )),

    MANAGER(Arrays.asList(
            CLIENT_READ, CLIENT_WRITE,
            PROJECT_READ, PROJECT_WRITE,
            FILE_READ, FILE_WRITE,
            USER_READ
    )),

    USER(Arrays.asList(
            CLIENT_READ,
            PROJECT_READ,
            FILE_READ,
            USER_READ
    ));

    private final List<UserPermission> permissions;

    UserRole(List<UserPermission> permissions) {
        this.permissions = permissions;
    }

    public List<UserPermission> getPermissions() {
        return permissions;
    }

    public List<SimpleGrantedAuthority> getGrantedAuthorities(){
        List<SimpleGrantedAuthority> permissions = getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toList());
        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}