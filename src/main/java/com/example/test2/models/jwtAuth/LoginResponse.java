package com.example.test2.models.jwtAuth;

import java.util.ArrayList;

public class LoginResponse {
    private Boolean successLogin;
    private String message;
    private String username;
    private String token;
    private ArrayList<String> authorities;

    public LoginResponse() {
    }

    public LoginResponse(Boolean successLogin, String message, String username, String token, ArrayList<String> authorities) {
        this.successLogin = successLogin;
        this.message = message;
        this.username = username;
        this.token = token;
        this.authorities = authorities;
    }

    public Boolean getSuccessLogin() {
        return successLogin;
    }

    public void setSuccessLogin(Boolean successLogin) {
        this.successLogin = successLogin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ArrayList<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(ArrayList<String> authorities) {
        this.authorities = authorities;
    }
}
