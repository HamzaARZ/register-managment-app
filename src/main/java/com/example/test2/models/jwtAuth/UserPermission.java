package com.example.test2.models.jwtAuth;

public enum UserPermission {
    CLIENT_READ("client:read"),
    CLIENT_WRITE("client:write"),
    PROJECT_READ("project:read"),
    PROJECT_WRITE("project:write"),
    FILE_READ("file:read"),
    FILE_WRITE("file:write"),
    USER_READ("user:read"),
    USER_WRITE("user:write");

    private final String permission;


    UserPermission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
