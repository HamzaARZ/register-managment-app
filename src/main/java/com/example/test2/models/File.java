package com.example.test2.models;



import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
//@Table(uniqueConstraints = {
//        @UniqueConstraint(columnNames = {"project", "fileName"}) -> PROJECT_ID
//})
public class File {

    @Id
    @Column(length = 36)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Type(type="uuid-char")
    private UUID id;

    private String fileName;

    @ManyToOne
    @JoinColumn(name = "PROJECT_ID")
    private Project project;


    public File(){}

    public File(UUID id, String fileName, Project project) {
        this.id = id;
        this.fileName = fileName;
        this.project = project;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public String toString() {
        return "File{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", project=" + project +
                '}';
    }
}