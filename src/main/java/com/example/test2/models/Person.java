package com.example.test2.models;


import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 36)
    @Type(type="uuid-char")
    private UUID id;
    private String nom;
    private String prenom;
    private Gender genre;
    private int age;


    public Person(){}

    public Person(UUID id, String nom, String prenom, Gender genre, int age){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.genre = genre;
        this.age = age;
    }



    //getters :

    public UUID getId() {return id;}
    public String getNom(){return nom;}
    public String getPrenom(){return prenom;}
    public Gender getGenre() { return genre;}
    public int getAge() {return age;}

    //setters :

    public void setId(UUID id) {this.id = id;}
    public void setNom(String nom){this.nom = nom;}
    public void setPrenom(String prenom) {this.prenom = prenom;}
    public void setAge(int age) {this.age = age;}
    public void setGenre(Gender genre) {this.genre = genre; }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", genre=" + genre +
                ", age=" + age +
                '}';
    }
}