package com.example.test2.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Entity
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PROJECT_ID", length = 36)
    @Type(type="uuid-char")
    private UUID id;
    private NatureProject natureProject;
    private Observation observation;
    private ProjectStatus projectStatus;
    private String architecte;

    @CreationTimestamp
    private LocalDateTime dateEntree;
    @UpdateTimestamp
    private LocalDateTime dateSortie;

    private Integer prixPaye;
    private Integer avance;
    private Integer reste;


    @ManyToOne
    @JoinColumn(name = "CLIENT_ID")
    private Client client;


    @OneToMany(fetch = FetchType.LAZY,mappedBy="project")
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JsonIgnore
    private Set<File> files;


    private LocalDateTime deletedAt;


    public Project(){}

    public Project(UUID id, NatureProject natureProject, Observation observation,
                   ProjectStatus projectStatus, String architecte, LocalDateTime dateEntree,
                   LocalDateTime dateSortie, Integer prixPaye, Integer avance, Integer reste,
                   Client client, Set<File> files) {
        this.id = id;
        this.natureProject = natureProject;
        this.observation = observation;
        this.projectStatus = projectStatus;
        this.architecte = architecte;
        this.dateEntree = dateEntree;
        this.dateSortie = dateSortie;
        this.prixPaye = prixPaye;
        this.avance = avance;
        this.reste = reste;
        this.client = client;
        this.files = files;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public NatureProject getNatureProject() {
        return natureProject;
    }

    public void setNatureProject(NatureProject natureProject) {
        this.natureProject = natureProject;
    }

    public Observation getObservation() {
        return observation;
    }

    public void setObservation(Observation observation) {
        this.observation = observation;
    }

    public ProjectStatus getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(ProjectStatus projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getArchitecte() {
        return architecte;
    }

    public void setArchitecte(String architecte) {
        this.architecte = architecte;
    }

    public LocalDateTime getDateEntree() {
        return dateEntree;
    }

    public void setDateEntree(LocalDateTime dateEntree) {
        this.dateEntree = dateEntree;
    }

    public LocalDateTime getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(LocalDateTime dateSortie) {
        this.dateSortie = dateSortie;
    }

    public Integer getPrixPaye() {
        return prixPaye;
    }

    public void setPrixPaye(Integer prixPaye) {
        this.prixPaye = prixPaye;
    }

    public Integer getAvance() {
        return avance;
    }

    public void setAvance(Integer avance) {
        this.avance = avance;
    }

    public Integer getReste() {
        return reste;
    }

    public void setReste(Integer reste) {
        this.reste = reste;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Set<File> getFiles() {
        return files;
    }

    public void setFiles(Set<File> files) {
        this.files = files;
    }

    public LocalDateTime getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(LocalDateTime deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", natureProject=" + natureProject +
                ", observation=" + observation +
                ", projectStatus=" + projectStatus +
                ", architecte='" + architecte + '\'' +
                ", dateEntree=" + dateEntree +
                ", dateSortie=" + dateSortie +
                ", prixPaye=" + prixPaye +
                ", avance=" + avance +
                ", reste=" + reste +
                ", client=" + client +
                ", files="  + files +
                '}';
    }
}
