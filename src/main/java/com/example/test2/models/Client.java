package com.example.test2.models;

import com.example.test2.validators.annotations.PhoneNumber;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;


@Entity
@Table(name="CLIENT")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="CLIENT_ID", length = 36)
    @Type(type="uuid-char")
    private UUID id;
    private String nom;
    private String prenom;
    @PhoneNumber
    private String numTelephone;

    @OneToMany(fetch = FetchType.LAZY,mappedBy="client")
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JsonIgnore
    private Set<Project> projects;


    private LocalDateTime deletedAt;

    public Client(){}

    public Client(UUID id, String nom, String prenom, String numTelephone, Set<Project> projects){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.numTelephone = numTelephone;
        this.projects = projects;
    }



    //getters :
    public UUID getId() {return id;}
    public String getNom(){return nom;}
    public String getPrenom(){return prenom;}
    public String getNumTelephone() {return numTelephone; }
    public Set<Project> getProjects() { return projects; }
    public LocalDateTime getDeletedAt() {
        return deletedAt;
    }

    //setters :
    public void setId(UUID id) {this.id = id;}
    public void setNom(String nom){this.nom = nom;}
    public void setPrenom(String prenom) {this.prenom = prenom;}
    public void setNumTelephone(String  numTelephone) {this.numTelephone = numTelephone; }
    public void setProjects(Set<Project> projects) { this.projects = projects; }
    public void setDeletedAt(LocalDateTime deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    public String toString() {
        return "Client id : " + id +  "\n\tnom : " + nom + "\n\tprenom : " + prenom + "\n\tnumTelephone : " + numTelephone +  "\n\tdleted at : " + deletedAt;
    }

}
