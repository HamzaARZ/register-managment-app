package com.example.test2.models;

public enum ProjectStatus {
    DONE,
    ON_GOING,
    PENDING
}