package com.example.test2.models;


import com.example.test2.models.jwtAuth.UserRole;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
public class User {
    @Id
    @NotBlank(message = "Username is required")
    @Size(min = 3, max = 18, message = "username must be between 3 and 18 characters long")
    private String username;

    @NotBlank(message = "Password is required")
    private String password;

    @NotNull
    private UserRole role;

    private Boolean enabled = false;


    public User(@NotBlank(message = "Username is required")
                @Size(min = 3, max = 20, message = "usernam must be between 3 and 20 characters long")
                        String username,
                @NotBlank(message = "Password is required") String password,
                @NotNull UserRole role,
                Boolean enabled) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.enabled = enabled;
    }

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", enabled=" + enabled +
                '}';
    }
}